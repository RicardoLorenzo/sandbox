CREATE KEYSPACE exchangerates;

USE exchangerates;

CREATE COLUMN FAMILY exchangerates
WITH comparator = UTF8Type
AND key_validation_class=UTF8Type
AND column_metadata = [
{column_name: id, validation_class: UTF8Type}
{column_name: date, validation_class: UTF8Type}
{column_name: currency, validation_class: UTF8Type}
{column_name: birth_year, validation_class: DoubleType}
];

UPDATE COLUMN FAMILY exchangerates
WITH comparator = UTF8Type
AND column_metadata = [{column_name: currency, validation_class: UTF8Type, index_type: KEYS}];
