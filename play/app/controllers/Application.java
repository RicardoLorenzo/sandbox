package controllers;

import java.util.List;

import models.ExchangeRate;

import org.springframework.beans.factory.annotation.Autowired;

import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Result;
import services.ExchangeRateService;

@org.springframework.stereotype.Controller
public class Application extends Controller {

    @Autowired
    private static ExchangeRateService exchangeService;

    public static Result index() {
        return ok(index.render());
    }

    /**
     * Async DB read of ExchangeRates
     * 
     * @param currency
     *            Currency filter
     * @return JSON response of ExchangeRate items
     */
    public static Result exchangeRates(final String currency) {
        List<ExchangeRate> rates = exchangeService.getAllExchangeRates(currency);
        if (rates == null) {
            return play.mvc.Controller.internalServerError("no rates retrieved");
        }
        return play.mvc.Controller.ok(Json.toJson(rates));
    }

    /**
     * Async pull of XML feed
     * 
     * @param currency
     *            Currency filter
     * @return 200 or 500 HTTP status codes
     */
    public static Result refresh(final String currency) {
        return exchangeService.update(currency);
    }
}
