package configs;

import org.springframework.context.annotation.Configuration;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Configuration
@EnableTransactionManagement
public class DataConfig {
    /*
     * @Bean
     * public EntityManagerFactory entityManagerFactory() {
     * HibernateJpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();
     * vendorAdapter.setShowSql(true);
     * vendorAdapter.setGenerateDdl(true);
     * LocalContainerEntityManagerFactoryBean entityManagerFactory = new LocalContainerEntityManagerFactoryBean();
     * entityManagerFactory.setPackagesToScan("models");
     * entityManagerFactory.setJpaVendorAdapter(vendorAdapter);
     * entityManagerFactory.setDataSource(dataSource());
     * entityManagerFactory.afterPropertiesSet();
     * return entityManagerFactory.getObject();
     * return null;
     * }
     * @Bean
     * public PlatformTransactionManager transactionManager() {
     * JpaTransactionManager transactionManager = new JpaTransactionManager(entityManagerFactory());
     * transactionManager.setDataSource(dataSource());
     * // transactionManager.setJpaDialect(new HibernateJpaDialect());
     * return transactionManager;
     * }
     * @Bean
     * public DataSource dataSource() {
     * final DriverManagerDataSource dataSource = new DriverManagerDataSource();
     * dataSource.setDriverClassName(Play.application().configuration().getString("db.default.driver"));
     * dataSource.setUrl(Play.application().configuration().getString("db.default.url"));
     * return dataSource;
     * }
     */

}