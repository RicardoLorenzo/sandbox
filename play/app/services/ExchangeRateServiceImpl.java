package services;

import java.util.List;

import models.ExchangeRate;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import play.libs.F.Function;
import play.libs.WS;
import play.mvc.Controller;
import play.mvc.Result;
import xml.ExchangeRateParser;
import db.CassandraClient;

@Service
@Transactional
public class ExchangeRateServiceImpl implements ExchangeRateService {

    // @PersistenceContext
    // EntityManager em;

    @Override
    public Result update(final String currency) {
        /**
         * FIXME Should we try to not create the presentation here
         */
        WS.url("http://www.ecb.europa.eu/stats/eurofxref/eurofxref-hist-90d.xml").get()
                .map(new Function<WS.Response, Result>() {
                    public Result apply(WS.Response response) {
                        if (updateDatabase(currency, response)) {
                            return Controller.ok();
                        }
                        return Controller.internalServerError();
                    }
                });
        return Controller.internalServerError();
    }

    @Override
    public List<ExchangeRate> getAllExchangeRates(final String currency) {
        CassandraClient db = new CassandraClient();
        return db.read(currency);
    }

    private static Boolean updateDatabase(String currency, WS.Response response) {
        // Parse XML
        ExchangeRateParser parser = new ExchangeRateParser();

        List<ExchangeRate> rates = parser.parseRates(response.getBodyAsStream(), currency);

        // Write to DB
        CassandraClient db = new CassandraClient();
        return db.write(currency, rates);
    }

}
