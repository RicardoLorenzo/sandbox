package services;

import java.util.List;

import models.ExchangeRate;
import play.mvc.Result;

public interface ExchangeRateService {

    /**
     * Updates Cassandra Database values retrieved from http://www.ecb.europa.eu/stats/eurofxref/eurofxref-hist-90d.xml.
     * 
     * @param currency
     *            Currency filter
     * @return Play MVC Result
     */
    public Result update(String currency);

    /**
     * Retrieves the <code>ExchangeRates</code> from Cassandra Database.
     * 
     * @param currency
     *            Currency filter
     * @return Play MVC Result
     */
    public List<ExchangeRate> getAllExchangeRates(String currency);

}