name := """TestApp"""

version := "1.0-SNAPSHOT"

libraryDependencies ++= Seq(
  javaCore,
  "com.h2database" % "h2" % "1.3.168",
  "org.springframework" % "spring-context" % "3.2.1.RELEASE",
  "org.springframework" % "spring-orm" % "3.2.1.RELEASE",
  "org.springframework" % "spring-jdbc" % "3.2.1.RELEASE",
  "org.springframework" % "spring-tx" % "3.2.1.RELEASE",
  "org.springframework" % "spring-expression" % "3.2.1.RELEASE",
  "org.springframework" % "spring-aop" % "3.2.1.RELEASE",
  "org.springframework" % "spring-test" % "3.2.1.RELEASE" % "test",
  "com.netflix.astyanax" % "astyanax" % "1.56.48",
  "com.netflix.astyanax" % "astyanax-core" % "1.56.48",
  "com.netflix.astyanax" % "astyanax-thrift" % "1.56.48",
  "com.netflix.astyanax" % "astyanax-entity-mapper" % "1.56.48",
  "com.netflix.astyanax" % "astyanax-cassandra" % "1.56.48",
  "cglib" % "cglib" % "2.2.2"
)

play.Project.playJavaSettings
