

import java.util.List;

import models.ExchangeRate;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractTransactionalJUnit4SpringContextTests;
import org.springframework.util.Assert;

import services.ExchangeRateService;
import configs.AppConfig;

@ContextConfiguration(classes = { AppConfig.class })
public class ExchangeRateServiceTest extends AbstractTransactionalJUnit4SpringContextTests {

    @Autowired
    ExchangeRateService exhangeService;

    @Test
    public void getExchangeRates() {
        String currency = "USD";
        List<ExchangeRate> rates = exhangeService.getAllExchangeRates(currency);
        Assert.notNull(rates);
        Assert.notEmpty(rates);
    }

}
